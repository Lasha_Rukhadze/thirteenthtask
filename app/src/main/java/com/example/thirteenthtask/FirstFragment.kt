package com.example.thirteenthtask

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thirteenthtask.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private lateinit var binding: FragmentFirstBinding
    private val viewModel : NewsViewModel by viewModels()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentFirstBinding.inflate(inflater, container, false)
        initializer()

        return binding.root
    }

    private fun initializer(){
        viewModel.init()
        recycler()
        observes()

        binding.refresh.setOnRefreshListener {
            adapter.clearData()
            viewModel.init()
        }
    }

    private fun recycler(){
        adapter = RecyclerViewAdapter()
        binding.recycler.layoutManager = LinearLayoutManager(requireActivity())
        binding.recycler.adapter = adapter
    }

    private fun observes(){
       /** viewModel.getLoadingLiveData().observe(viewLifecycleOwner, {
            binding.refresh.isRefreshing = it
        })*/
        viewModel.getNewsLiveData().observe(viewLifecycleOwner,{

            when(it.status){
                Result.Status.SUCCESS -> {
                    it.data?.let { it1 -> adapter.setter(it1.toMutableList()) }
                }
                Result.Status.ERROR -> {
                    Toast.makeText(requireActivity(), it.msg, Toast.LENGTH_LONG).show()
                }
                Result.Status.LOADING -> {
                    binding.refresh.isRefreshing = !binding.refresh.isRefreshing
                }
            }
        })
    }


}