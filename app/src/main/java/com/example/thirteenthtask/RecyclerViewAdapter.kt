package com.example.thirteenthtask

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thirteenthtask.databinding.NewsBinding

class RecyclerViewAdapter() : RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {
    private val news = mutableListOf<NewsModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(NewsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int {
        return news.size
    }

    inner class ItemViewHolder(private val binding : NewsBinding) : RecyclerView.ViewHolder(binding.root){
        private lateinit var model: NewsModel
        fun bind(){
            model = news[absoluteAdapterPosition]
        }
    }

    fun setter(news : MutableList<NewsModel>){
        this.news.clear()
        this.news.addAll(news)
    }

    fun clearData(){
        this.news.clear()
        notifyDataSetChanged()
    }
}