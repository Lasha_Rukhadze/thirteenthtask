package com.example.thirteenthtask

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewsViewModel : ViewModel() {

    private val newsLiveData = MutableLiveData<Result<List<NewsModel>>>().apply {
        mutableListOf<NewsModel>()
    }

    //val _newsLiveData : LiveData<List<NewsModel>> = newsLiveData
    fun getNewsLiveData() : LiveData<Result<List<NewsModel>>> {
        return newsLiveData
    }


    /**private val loadingLiveData = MutableLiveData<Boolean>().apply {
        true
    }

    //val _loadingLiveData : LiveData<Boolean> = loadingLiveData
    fun getLoadingLiveData() : LiveData<Boolean>{
        return loadingLiveData
    }*/

    fun init(){
        CoroutineScope(Dispatchers.IO).launch {
            getNewss()
        }
    }

    private suspend fun getNewss(){
        newsLiveData.postValue(Result.loading())
        val response = RetrofitService.retrofitService().getNews()
        if (response.isSuccessful) {
            val news = response.body()
            newsLiveData.postValue(Result.success(news!!))
        } else {
            response.errorBody()
            newsLiveData.postValue(Result.error(response.message()))
        }
       // loadingLiveData.postValue(false)
    }
}