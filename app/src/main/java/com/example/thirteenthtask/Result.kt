package com.example.thirteenthtask

data class Result <T> (val status : Status, val data : T? =null, val msg : String?=null, val loading : Boolean? =true){

    enum class Status{
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {

        fun <T> success(data : T) : Result<T> {
            return Result(Status.SUCCESS, data, null, false)
        }

        fun <T> error(msg : String) : Result<T> {
            return Result(Status.ERROR, null, msg, false)
        }

        fun <T> loading() :Result<T> {
            return Result(Status.LOADING)
        }

    }
}
