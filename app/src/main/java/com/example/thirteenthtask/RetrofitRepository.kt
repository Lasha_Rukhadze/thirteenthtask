package com.example.thirteenthtask

import retrofit2.Response
import retrofit2.http.GET

interface RetrofitRepository {
        @GET("/api/m/v2/news/")
        suspend fun getNews() : Response<List<NewsModel>>
}