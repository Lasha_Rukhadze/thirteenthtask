package com.example.thirteenthtask

import com.google.gson.annotations.SerializedName

data class NewsModel(val id: Int,
                     val descriptionEN: String,
                     val descriptionKA: String,
                     val descriptionRU: String,
                     val titleEN: String,
                     val titleKA: String,
                     val titleRU: String,

                     @SerializedName("created_at")
                     val createdAt: Long,

                     @SerializedName("updated_at")
                     val updatedAt: Long,

                     val cover: String,
                     var isLast: Boolean = false){

}
