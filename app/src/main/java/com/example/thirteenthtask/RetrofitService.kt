package com.example.thirteenthtask

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    private const val BASE_URL = "http://139.162.207.17"

    fun retrofitService() : RetrofitRepository {
        return Retrofit.Builder().baseUrl(
            BASE_URL
        ).addConverterFactory(GsonConverterFactory.create()).build().create(RetrofitRepository::class.java)
    }
}