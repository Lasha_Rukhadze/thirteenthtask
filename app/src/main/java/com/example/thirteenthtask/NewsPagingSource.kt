package com.example.thirteenthtask

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingSource
import androidx.paging.PagingState

class NewsPagingSource () : PagingSource<Int, NewsModel>(){

    private companion object {
        const val INITIAL_PAGE_INDEX = 0
    }

    override fun getRefreshKey(state: PagingState<Int, NewsModel>): Int? = null

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NewsModel> {
        val position = params.key ?: INITIAL_PAGE_INDEX
        //...
    }
}